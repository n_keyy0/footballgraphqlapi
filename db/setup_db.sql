BEGIN;
-------------
-- INIT DB --
-------------

DO
$do$
BEGIN
  IF NOT EXISTS (
      SELECT
      FROM pg_database
      where datname = 'football'
    ) THEN

    CREATE DATABASE football;
  END IF;
  IF NOT EXISTS (
    SELECT
    FROM   pg_catalog.pg_roles
    WHERE  rolname = 'football_admin') THEN

    CREATE ROLE football_admin WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB NOREPLICATION LOGIN PASSWORD 'password';
    GRANT azure_pg_admin TO mentor_dba;
    ALTER DATABASE football OWNER TO football_admin;
  END IF;


--Tables--

-- CREATE TABLE football_club (
--     id serial PRIMARY KEY,
--     name varchar(70) NOT NULL,
--     year int NOT NULL
-- );

-- ALTER TABLE football_club OWNER TO football_admin;

END
$do$;

COMMIT;