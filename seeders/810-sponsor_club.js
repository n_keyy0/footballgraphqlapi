/* eslint-disable camelcase */

'use strict'

const _ = require('lodash')
const { seedData: teamData } = require('./690-team')
const { seedData: sponsorData } = require('./780-sponsor')

const dataArr = [];
(() => {
  for (let i = 0; i < 600; i++) {
    dataArr.push({
      sponsor_id: _.sample(sponsorData).id,
      team_id: _.sample(teamData).id,
    })
  }
})()

const data = _.uniqBy(
  dataArr,
  ({ sponsor_id, team_id }) => `${sponsor_id} ${team_id}`,
)

module.exports = {
  seedData: data,
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('sponsor_club', data)
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('sponsor_club', null, {})
  },
}
