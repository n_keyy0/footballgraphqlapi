'use strict'

const { v4 } = require('uuid')
const faker = require('faker')
const _ = require('lodash')
const { seedData: countriesData } = require('./470-country')

const data = [];
(() => {
  for (let i = 0; i < 100; i++) {
    data.push({
      id: v4(),
      name: faker.name.firstName(),
      surname: faker.name.lastName(),
      image: faker.image.imageUrl(640, 480),
      country_id: _.sample(countriesData).id,
    })
  }
})()

module.exports = {
  seedData: data,
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('referee', data)
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('referee', null, {})
  },
}
