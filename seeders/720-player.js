'use strict'

const { v4 } = require('uuid')
const faker = require('faker')
const _ = require('lodash')
const { seedData: citiesData } = require('./510-city')
const { seedData: positionsData } = require('./570-player_position')
const { seedData: teamData } = require('./690-team')

const data = [];
(() => {
  for (let i = 0; i < 800; i++) {
    data.push({
      id: v4(),
      name: faker.name.firstName(),
      surname: faker.name.lastName(),
      image: faker.image.imageUrl(640, 480),
      goals_scored: _.random(0, 50),
      assists: _.random(0, 30),
      shirt_number: _.random(1, 99),
      games_played: _.random(0, 38),
      // eslint-disable-next-line max-len
      birth_date: faker.date.between(new Date(10 * 365 * 24 * 3600 * 1000), new Date(35 * 365 * 24 * 3600 * 1000)),
      is_injured: _.sample([true, false]),
      team_id: _.sample(teamData).id,
      position_id: _.sample(positionsData).id,
      city_id: _.sample(citiesData).id,
    })
  }
})()

module.exports = {
  seedData: data,
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('player', data)
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('player', null, {})
  },
}
