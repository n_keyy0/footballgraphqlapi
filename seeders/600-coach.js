'use strict'

const { v4 } = require('uuid')
const faker = require('faker')
const _ = require('lodash')
const { seedData: citiesData } = require('./510-city')

const data = [];
(() => {
  for (let i = 0; i < 800; i++) {
    data.push({
      id: v4(),
      name: faker.name.firstName(),
      surname: faker.name.lastName(),
      image: faker.image.imageUrl(640, 480),
      city_id: _.sample(citiesData).id,
    })
  }
})()

module.exports = {
  seedData: data,
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('coach', data)
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('coach', null, {})
  },
}
