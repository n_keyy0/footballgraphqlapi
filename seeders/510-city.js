'use strict'

const { v4 } = require('uuid')
const faker = require('faker')
const _ = require('lodash')
const { seedData: countriesData } = require('./470-country')

const data = [];
(() => {
  for (let i = 0; i < 1000; i++) {
    const name = faker.address.city()
    const abbr = name.slice(0, 3)
    data.push({
      id: v4(),
      name,
      abbr,
      country_id: _.sample(countriesData).id,
    })
  }
})()

module.exports = {
  seedData: data,
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('city', data)
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('country', null, {})
  },
}
