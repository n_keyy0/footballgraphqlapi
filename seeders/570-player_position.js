'use strict'

const { v4 } = require('uuid')

const data = [
  {
    id: v4(),
    description: 'Goalkeeper',
    abbr: 'GK',
  },
  {
    id: v4(),
    description: 'Centre-Back',
    abbr: 'CB',
  },
  {
    id: v4(),
    description: 'Right-Back',
    abbr: 'RB',
  },
  {
    id: v4(),
    description: 'Left-Back',
    abbr: 'LB',
  },
  {
    id: v4(),
    description: 'Right-Wing-Back',
    abbr: 'RWB',
  },
  {
    id: v4(),
    description: 'Left-Wing-Back',
    abbr: 'LWB',
  },
  {
    id: v4(),
    description: 'Center-Defensive-Midfielder',
    abbr: 'CDM',
  },
  {
    id: v4(),
    description: 'Center-Midfielder',
    abbr: 'CM',
  },
  {
    id: v4(),
    description: 'Center-Attacking-Midfielder',
    abbr: 'CAM',
  },
  {
    id: v4(),
    description: 'Right-Midfielder',
    abbr: 'RM',
  },
  {
    id: v4(),
    description: 'Left-Midfielder',
    abbr: 'LM',
  },
  {
    id: v4(),
    description: 'Right-Wing',
    abbr: 'RW',
  },
  {
    id: v4(),
    description: 'Left-Wing',
    abbr: 'LW',
  },
  {
    id: v4(),
    description: 'Center-Forward',
    abbr: 'CF',
  },
  {
    id: v4(),
    description: 'Striker',
    abbr: 'ST',
  },
]

module.exports = {
  seedData: data,
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('player_position', data)
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('player_position', null, {})
  },
}
