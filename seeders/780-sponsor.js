'use strict'

const { v4 } = require('uuid')
const faker = require('faker')
const _ = require('lodash')
const { seedData: citiesData } = require('./510-city')

const data = [];
(() => {
  for (let i = 0; i < 200; i++) {
    data.push({
      id: v4(),
      description: faker.company.companyName(),
      city_id: _.sample(citiesData).id,
    })
  }
})()

module.exports = {
  seedData: data,
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('sponsor', data)
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('sponsor', null, {})
  },
}
