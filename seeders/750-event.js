'use strict'

const { v4 } = require('uuid')
const faker = require('faker')
const _ = require('lodash')
const { seedData: tournamentData } = require('./630-championship')
const { seedData: stadiumData } = require('./540-stadium')
const { seedData: teamsData } = require('./690-team')

const data = [];
(() => {
  for (let i = 0; i < 800; i++) {
    data.push({
      id: v4(),
      score: `${_.random(0, 10)}: ${_.random(0, 10)}`,
      date_time: faker.date.past(1),
      tournament_id: _.sample(tournamentData).id,
      stadium_id: _.sample(stadiumData).id,
      first_team_id: _.sample(teamsData).id,
      second_team_id: _.sample(teamsData).id,
    })
  }
})()

module.exports = {
  seedData: data,
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('event', data)
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('event', null, {})
  },
}
