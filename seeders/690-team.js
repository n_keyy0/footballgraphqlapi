/* eslint-disable camelcase */

'use strict'

const { v4 } = require('uuid')
const faker = require('faker')
const _ = require('lodash')
const { seedData: stadiumData } = require('./540-stadium')
const { seedData: headCoachData } = require('./600-coach')
const { seedData: championshipData } = require('./630-championship')
const { seedData: citiesData } = require('./510-city')

const dataArr = [];
(() => {
  for (let i = 0; i < 800; i++) {
    const goals_conceded = _.random(0, 150)
    const goals_scored = _.random(0, 150)
    const goals_diff = goals_scored - goals_conceded
    dataArr.push({
      id: v4(),
      name: faker.address.city(),
      year_foundation: _.random(1840, 2010),
      image: faker.image.imageUrl(640, 480),
      place_in_table: _.random(1, 20),
      goals_scored,
      goals_conceded,
      goals_diff,
      points: _.random(0, 127),
      games_played: _.random(0, 38),
      stadium_id: _.sample(stadiumData).id,
      head_coach_id: headCoachData[i].id,
      championship_id: _.sample(championshipData).id,
      city_id: _.sample(citiesData).id,
    })
  }
})()

const data = _.uniqBy(
  dataArr,
  ({ place_in_table, championship_id }) => `${place_in_table} ${championship_id}`,
)

module.exports = {
  seedData: data,
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('team', data)
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('team', null, {})
  },
}
