'use strict'

const { v4 } = require('uuid')
const faker = require('faker')

const data = []
const seedData = () => {
  for (let i = 0; i < 100; i++) {
    const country = faker.address.country()
    const abbr = country.slice(0, 3).toUpperCase()
    data.push({
      id: v4(),
      name: country,
      abbr,
    })
  }
}

seedData()

module.exports = {
  seedData: data,
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('country', data)
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('country', null, {})
  },
}
