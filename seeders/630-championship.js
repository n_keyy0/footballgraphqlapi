'use strict'

const { v4 } = require('uuid')
const _ = require('lodash')
const { seedData: countriesData } = require('./470-country')

const data = [
  {
    id: v4(),
    name: 'English Premier League',
    country_id: _.sample(countriesData).id,
  },
  {
    id: v4(),
    name: 'La Liga',
    country_id: _.sample(countriesData).id,
  },
  {
    id: v4(),
    name: 'Russian Premier League',
    country_id: _.sample(countriesData).id,
  },
  {
    id: v4(),
    name: 'Bundesliga',
    country_id: _.sample(countriesData).id,
  },
  {
    id: v4(),
    name: 'League 1',
    country_id: _.sample(countriesData).id,
  },
]

module.exports = {
  seedData: data,
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('championship', data)
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('championship', null, {})
  },
}
