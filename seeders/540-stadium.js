'use strict'

const { v4 } = require('uuid')
const faker = require('faker')
const _ = require('lodash')
const { seedData: citiesData } = require('./510-city')

const data = []
const hasRoofData = [true, false];
(() => {
  for (let i = 0; i < 1000; i++) {
    const name = faker.address.streetName()
    data.push({
      id: v4(),
      name,
      capacity: _.random(1000, 85000),
      city_id: _.sample(citiesData).id,
      has_roof: _.sample(hasRoofData),
    })
  }
})()

module.exports = {
  seedData: data,
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('stadium', data)
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('stadium', null, {})
  },
}
