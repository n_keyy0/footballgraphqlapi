const Sequelize = require('sequelize')
const sequelize = require('../config/db')

const Country = sequelize.define(
  'country',
  {
    id: {
      type: Sequelize.UUIDV4,
      primaryKey: true,
      allowNull: false,
    },
    name: {
      type: Sequelize.STRING(70),
      allowNull: false,
    },
    abbr: {
      type: Sequelize.STRING(4),
      allowNull: false,
    },
  },
  { timestamps: false, freezeTableName: true },
)

module.exports = Country
