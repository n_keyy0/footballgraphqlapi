const express = require('express')
require('dotenv').config()
const { ApolloServer } = require('apollo-server-express')
const cors = require('cors')
const typeDefs = require('./graphql/schema')
const resolvers = require('./graphql/resolvers')
const sequelize = require('./config/db')
const models = require('./models/index')

const PORT = process.env.PORT

async function startApolloServer() {
  try {
    await sequelize.authenticate()
    // eslint-disable-next-line no-console
    console.log('Database connected...')
    const server = new ApolloServer({
      typeDefs,
      resolvers,
      context: { models },
    })
    await server.start()
    const app = express()
    app.use(cors())
    server.applyMiddleware({ app })
    await new Promise((resolve) => app.listen({ port: PORT }, resolve))

    // eslint-disable-next-line no-console
    console.log(
      `🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`,
    )
    return { server, app }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(`Something went wrong. Error: ${error}`)
    return error
  }
}

startApolloServer()
