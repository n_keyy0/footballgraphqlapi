const { mergeResolvers } = require('graphql-tools')
const CountryResolver = require('./Country/resolver')

module.exports = mergeResolvers([
  CountryResolver,
])
