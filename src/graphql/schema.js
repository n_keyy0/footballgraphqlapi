const { mergeTypeDefs } = require('graphql-tools')
const CountryType = require('./Country/schema')
const CityType = require('./City/schema')

module.exports = mergeTypeDefs([
  CountryType,
  CityType,
])
