const getAllCountries = require('../../repository/Country/getAllCountries')

module.exports = {
  Query: {
    getAllCountries: async (_parent, _args, { models }) => {
      try {
        return await getAllCountries(models)
      } catch (error) {
        throw new Error(error)
      }
    },
  },
}
