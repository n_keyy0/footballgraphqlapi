const typeDefs = `
type Country {
    id: ID!,
    name: String!
    abbr: String!
}

type Query {
    getAllCountries(
        page: Int,
        perPage: Int,
        sortField: String,
        sortOrder: String,
        filter: CountryFilter
    ): [Country]!
},

input CountryFilter {
    q: String,
    id: ID,
    name: String,
    abbr: String
}
`

module.exports = typeDefs
