const typeDefs = `
type City {
    id: ID!,
    name: String!
    abbr: String!
    country_id: ID!
}
`

module.exports = typeDefs
