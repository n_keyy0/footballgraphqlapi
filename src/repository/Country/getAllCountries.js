module.exports = async (models) => {
  try {
    return await models.Country.findAll({ raw: true })
  } catch (error) {
    throw Error(error)
  }
}
