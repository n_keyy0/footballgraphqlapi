'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('sponsor', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      country_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'country',
            schema: 'public',
          },
          key: 'id',
        },
      },
      city_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'city',
            schema: 'public',
          },
          key: 'id',
        },
      },
    })
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('sponsor')
  },
}
