'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('stadium', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(70),
        allowNull: false,
      },
      capacity: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      has_roof: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      city_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'city',
            schema: 'public',
          },
          key: 'id',
        },
        allowNull: false,
      },
    })
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('stadium')
  },
}
