'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('player', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      surname: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      image: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      goals_scored: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      assists: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      shirt_number: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      games_played: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      birth_date: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      is_injured: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      team_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'team',
            schema: 'public',
          },
          key: 'id',
        },
      },
      position_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'player_position',
            schema: 'public',
          },
          key: 'id',
        },
      },
      city_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'city',
            schema: 'public',
          },
          key: 'id',
        },
        allowNull: true,
      },
    })
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('player')
  },
}
