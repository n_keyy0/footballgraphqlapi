'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('championship', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(70),
        allowNull: false,
      },
      country_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'country',
            schema: 'public',
          },
          key: 'id',
        },
        allowNull: false,
      },
    })
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('championship')
  },
}
