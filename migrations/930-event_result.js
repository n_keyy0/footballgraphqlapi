'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('event_result', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
      },
      result_type: {
        type: Sequelize.ENUM('W1', 'W2', 'D'),
      },
      win_team_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'team',
            schema: 'public',
          },
          key: 'id',
        },
        allowNull: true,
      },
    })
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('event_result')
  },
}
