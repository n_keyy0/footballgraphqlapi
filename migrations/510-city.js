'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('city', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(70),
        allowNull: false,
      },
      abbr: {
        type: Sequelize.STRING(4),
        allowNull: false,
      },
      country_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'country',
            schema: 'public',
          },
          key: 'id',
        },
        allowNull: false,
      },
    })
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('city')
  },
}
