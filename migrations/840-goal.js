'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('goal', {
      id: {
        type: Sequelize.UUID,
        allowNull: false,
      },
      goal_time: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      goal_half: {
        type: Sequelize.INTEGER,
      },
      event_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'event',
            schema: 'public',
          },
          key: 'id',
        },
      },
      player_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'player',
            schema: 'public',
          },
          key: 'id',
        },
      },
      team_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'team',
            schema: 'public',
          },
          key: 'id',
        },
      },
    })
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('goal')
  },
}
