'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('trophy_team', {
      team_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'team',
            schema: 'public',
          },
          key: 'id',
        },
        allowNull: false,
      },
      trophy_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'trophy',
            schema: 'public',
          },
          key: 'id',
        },
        allowNull: false,
      },
    })

    await queryInterface.addConstraint('trophy_team', {
      fields: ['trophy_id', 'team_id'],
      type: 'primary key',
      name: 'trophy_team_pkey',
    })
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('trophy')
  },
}
