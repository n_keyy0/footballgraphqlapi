'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('team', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      year_foundation: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      image: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      place_in_table: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      goals_scored: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      goals_conceded: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0,
      },
      goals_diff: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0,
      },
      points: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      games_played: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      stadium_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'stadium',
            schema: 'public',
          },
          key: 'id',
        },
        allowNull: false,
      },
      head_coach_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'coach',
            schema: 'public',
          },
          key: 'id',
        },
      },
      championship_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'championship',
            schema: 'public',
          },
          key: 'id',
        },
      },
      city_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'city',
            schema: 'public',
          },
          key: 'id',
        },
        allowNull: true,
      },
    })
    await queryInterface.addConstraint('team', {
      fields: ['place_in_table', 'championship_id'],
      type: 'unique',
      name: 'unique_place_in_championship',
    })
    await queryInterface.addConstraint('team', {
      fields: ['head_coach_id'],
      type: 'unique',
      name: 'unique_coach',
    })
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('team')
  },
}
