'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('sponsor_club', {
      sponsor_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'sponsor',
            schema: 'public',
          },
          key: 'id',
        },
      },
      team_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'team',
            schema: 'public',
          },
          key: 'id',
        },
      },
    })

    await queryInterface.addConstraint('sponsor_club', {
      fields: ['sponsor_id', 'team_id'],
      type: 'primary key',
      name: 'sponsor_team_pkey',
    })
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('sponsor_club')
  },
}
