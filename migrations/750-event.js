'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('event', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
      },
      score: {
        type: Sequelize.STRING(70),
        allowNull: false,
      },
      date_time: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      tournament_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'championship',
            schema: 'public',
          },
          key: 'id',
        },
      },
      stadium_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'stadium',
            schema: 'public',
          },
          key: 'id',
        },
        allowNull: false,
      },
      first_team_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'team',
            schema: 'public',
          },
          key: 'id',
        },
      },
      second_team_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'team',
            schema: 'public',
          },
          key: 'id',
        },
      },
    })
    await queryInterface.addConstraint('event', {
      fields: ['first_team_id', 'second_team_id', 'date_time'],
      type: 'unique',
      name: 'unique_events_clubs_and_date',
    })
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('event')
  },
}
