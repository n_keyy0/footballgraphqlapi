'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('player_position', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
      },
      description: {
        type: Sequelize.STRING(40),
        allowNull: false,
      },
      abbr: {
        type: Sequelize.STRING(4),
        allowNull: false,
      },
    })
    await queryInterface.addConstraint('player_position', {
      fields: ['abbr'],
      type: 'unique',
      name: 'unique_abbr',
    })
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('player_position')
  },
}
