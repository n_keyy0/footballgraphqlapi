'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('coach', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      surname: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      image: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      city_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'city',
            schema: 'public',
          },
          key: 'id',
        },
      },
    })
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('coach')
  },
}
